
// General 
var color_primary = getComputedStyle(document.body).getPropertyValue("--primary");

/*
===   Map   ==========================================================
*/

// Init map
mapboxgl.accessToken = mapbox_accessToken;
var map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/outdoors-v9",
  // starting position [lng, lat]
  center: [10.965507, 47.423258], 
  // starting zoom
  zoom: 13, 
  pitchWithRotate: false,
  keyboard: false
});

// add map controlls
map.addControl(new mapboxgl.ScaleControl(), "bottom-right");
map.addControl(new mapboxgl.NavigationControl(), "bottom-right");
// map.addControl(new MapboxGeocoder({ accessToken: mapboxgl.accessToken }), "top-right");


/*
=== Map UI ===================================
*/

// GeoJSON object to hold our measurement features
var geojson = {
  "type": "FeatureCollection",
  "features": []
};


map.on("load", function () {

  // Hide unnecessary layers from outdoors style
  var hide_layers = ["housenum-label", "poi-relevant-scalerank4-l15", "poi-relevant-scalerank4-l1", "poi-parks_scalerank4", "poi-scalerank3", "poi-parks-scalerank3", "road-shields-black", "road-shields-white", "motorway-junction", "poi-outdoor-features"];
  hide_layers.forEach(function (layer) {
    map.setLayoutProperty(layer, "visibility", "none");
  });

  // Add source for clicked line
  map.addSource("geojson", {
    "type": "geojson",
    "data": geojson
  });

  // Add styles to the map
  map.addLayer({
    id: "measure-points",
    type: "circle",
    source: "geojson",
    paint: {
      "circle-radius": 4,
      "circle-color": "#ffffff",
      "circle-stroke-width": 4,
      "circle-stroke-color": color_primary
    },
    filter: ["in", "$type", "Point"]
  });

  map.on("click", function (e) {
    var features = map.queryRenderedFeatures(e.point, { layers: ["measure-points"] });

    // Remove the point from the group
    // So we can redraw it based on the collection
    if (geojson.features.length > 0) geojson.features.pop();


    // If a feature was clicked, remove it from the map
    if (features.length) {
      var id = features[0].properties.id;
      geojson.features = geojson.features.filter(function (point) {
        return point.properties.id !== id;
      });

    } else {
      // Else add point
      var pointDisplay = {
        "type": "Feature",
        "geometry": {
          "type": "Point",
          "coordinates": [e.lngLat.lng, e.lngLat.lat]
        },
        "properties": {
          "id": String(new Date().getTime())
        }
      };

      geojson.features.push(pointDisplay);
      // console.log(geojson.features);
    }
    map.getSource("geojson").setData(geojson);
  }); // map on click

  map.on("mousemove", function (e) {
    var features = map.queryRenderedFeatures(e.point, { layers: ["measure-points"] });
    // UI indicator for clicking/hovering a point on the map
    map.getCanvas().style.cursor = features.length ? "pointer" : "crosshair";
  }); // map on mousemove

}); // map on load







/*
=== Coordinates and Elevation ============================================
*/

var lngDisplay = document.getElementById('lng');
var latDisplay = document.getElementById('lat');
var eleDisplay = document.getElementById('ele');
var mHDisplay = document.getElementById('me-height');
var mPDisplay = document.getElementById('me-pos');
var btn_clear = document.getElementById('ka-clear');

// Create variables for the latitude, longitude, elevation
var lng;
var lat;
var point;
var points;
var dis;
var elev = [];


map.on('click', function(e) {
  // When the map is clicked, set variables equal to the lng and lat properties in the returned lngLat object
  lng = e.lngLat.lng;
  lat = e.lngLat.lat;


  point = e.lngLat;
  // get Bounding Box with SW and NE
  points = point.toBounds(20);
  // Convert to mapbox object
  ne = mapboxgl.LngLat.convert(points._ne); sw = mapboxgl.LngLat.convert(points._sw);
  // calculate Distance from SW and NE (Mapbox v1.11.0)
  dis = ne.distanceTo(sw);
  points = points.toArray();

  // Points in order: SWBox, Center, NEBox
  points.splice(0, 0, point.toArray()); 

  requestElevation();
});

const elevation = new Elevation(lng, lat, points);

async function requestElevation() {
  $(document).ajaxStart(function() { $(elevation_loading).css("visibility", "visible"); });
  $(document).ajaxStop(function() { $(elevation_loading).css("visibility", "hidden"); });

    // Await Elevation request from class Elevation
    await elevation.getOpenTopoData(points)
    .then(e => {

    // Check if Array is full and empty before new location
    if(Array.isArray(elev) && elev.length){
      elev = [];
      e.results.forEach((ele) => { elev.push(ele.elevation) });
      console.log('Changed: ' + elev);
    } else {
      e.results.forEach((ele) => { elev.push(ele.elevation) });
      console.log('First: ' + elev);
    };

    // Dislay coordinates and elevation
    lngDisplay.textContent = lng.toFixed(3);
    latDisplay.textContent = lat.toFixed(3);
    eleDisplay.textContent = elev[0] + ' meters';
    
    // Calculate Koppe mean error wit elevation value
    calcMeanError(elev);

    })
    .catch(err => alert("\n⚠️ Request for elevation failed:\n Error of" + err.error))
    
};// End of "click", further calcMeanError beneath




/*
===   Scale Factor Select   ===================================================
*/
var scale;
var ccode = {
  o: [''], 
  A: ['A', 10000, 1, 3],
  CH: ['CH', 50000, 1.5, 10], 
  USA: ['USA', 50000, 1.8, 15]
}

var select = document.getElementById("ccode-select");

// Show option for country code
Object.values(ccode).forEach((code)=>{
  var opt = document.createElement("option");
  opt.text = code[0];
  select.add(opt);
})
// Handle selection of other scale and factor
$('#ccode-select').change(function (e) {
  scale = e.target.value; 
  // Calculate elevation with new scale
  calcMeanError(elev);
  return scale;
});


/*
=== Calculate Koppe Mean Error =================================================
*/

var mH;
var mP;

function calcMeanError(elev){
  var elevDiff = [...elev].pop() - elev[elev.length - 2];
  var facA;
  var facB;
  // Calculate Elevation Angle
  alpha = Math.atan((elevDiff) / (dis));

  // Factors while scale is changed
  switch (scale) {
    case 'A':
      facA = ccode[scale][2];
      facB = ccode[scale][3];
    break;
    case 'CH':
      facA = ccode[scale][2];
      facB = ccode[scale][3];
    break;
    case 'USA':
      facA = ccode[scale][2];
      facB = ccode[scale][3];
    break;
    default:
      // Alert
      alert('No Scale Factor is Chosen! \n  ⚠️Please select beneath map.')
  }

  if(facA !== undefined){
    // Calculate Mean Errors
    mH = facA + facB * Math.tan(alpha);
    mP = facB + facA *(1 / Math.tan(alpha));
    // Display Results
    updateMEDisplay(mH, mP);
  }
};


function updateMEDisplay(mH, mP){
  mHDisplay.textContent = mH.toFixed(3) + ' m';
  mPDisplay.textContent = mP.toFixed(3) + ' m';
}

function clear(){
  // Clear Map Location of geojson
  geojson.features = [];
  map.getSource("geojson").setData(geojson);
  // Empty values of all variable
  mH, mP, elev, lng, lat = [];
  // Empty Text Content
  lngDisplay.textContent = [];
  latDisplay.textContent = [];
  eleDisplay.textContent = [];
  mHDisplay.textContent = [];
  mPDisplay.textContent = [];
};

btn_clear.addEventListener("click", function (e) {clear();});
