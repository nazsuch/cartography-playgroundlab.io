//==============================================================================
//  General
//==============================================================================

// extension of the viewbox
var size_x = 500;
var size_y = 300;

var diameter = 6; // diameter of circle

var draw = SVG("drawing");
draw.viewbox(0, 0, size_x, size_y);

var dataColor = "#607D8B"; // Blue Grey
var dataPoints = [];

draw.node.classList.add("cursor-crosshair"); // when cursor inside viewbox --> displayed as cross hairs

// setup of booleans
var drawingVoronoi = true;
var drawingDelaunay = false;

// setup for voronoi
pointsMove = [];
v = new Voronoi();

// connection to the html
var check_voronoi = document.getElementById("check-voronoi");
var check_delaunay = document.getElementById("check-delaunay");	
var dtvd_button_clear = document.getElementById("dtvd-clear");
var dtvd_button_reset = document.getElementById("dtvd-reset");


//==============================================================================
//  Setup
//==============================================================================

// clicking on the container creates a new fixed point and triggers a redraw
draw.node.addEventListener("click", function (e) {addNewPoint(getPointClickedOn(e));}, false);

// when mouse is moved on the container, we show a point at mousePointerPosition and calculate Voronoi considering this moving point
draw.node.addEventListener("mousemove", function (e) {addMovePoint(getPointClickedOn(e));}, false);

// when mouse leaves the container, we remove the moving point
draw.node.addEventListener("mouseleave", function (e) {removeMovePoint(getPointClickedOn(e));}, false);

// when clear button is clicked, load empty dataset
dtvd_button_clear.addEventListener("click", function () {loadData(default_points2);}, false);

// when reset button is clicked, load default dataset
dtvd_button_reset.addEventListener("click", function () {loadData(default_points);}, false);

// when checkbox changes, change corresponding boolean
check_voronoi.addEventListener("change", function () {toggleVornoi();}, false);
check_delaunay.addEventListener("change", function () {toggleDelaunay();}, false);

// on load, load default dataset
window.addEventListener('load', function () {loadData(default_points);}, false);


//==============================================================================
//  Data
//==============================================================================

var default_points = [{x:62,y:83}, {x:126,y:224}, {x:439,y:178}, {x:370,y:68}, {x:250,y:150}, {x:380,y:256}];
var default_points2 = [];


//==============================================================================
//  Functionality
//==============================================================================

// Data Loading

function loadData(data) {
  dataPoints = copy(data);	
  redrawAll();
}

// Toggle Handling

function toggleVornoi() {
	drawingVoronoi = !drawingVoronoi;
	redrawAll();
}

function toggleDelaunay() {
	drawingDelaunay = !drawingDelaunay;
	redrawAll();
}

// Getting/Adding/Removing Points

function getPointClickedOn(e) {
  // cursor point in display coordinates
  var pt_client = draw.node.createSVGPoint();
  pt_client.x = e.clientX;
  pt_client.y = e.clientY;
  // cursor point, translated into svg coordinates
  var pt_svg = pt_client.matrixTransform(draw.node.getScreenCTM().inverse());
  return {x:pt_svg.x, y:pt_svg.y};
}

function addNewPoint(pt) {
    // current point becomes a fixed point
	dataPoints.push(pt);
	//remove current point from movingPoint-array
	if (pointsMove.length == 1) {
		pointsMove.pop();
	}
    redrawAll();	
}

function addMovePoint(pt) {
	// current point becomes the new moving point; remove previous point from movingPoint-array
	if (pointsMove.length == 1) {
		pointsMove.pop();
	}
	pointsMove.push( new Point(pt.x, pt.y));
	redrawAll();
}

function removeMovePoint(pt) {
	if (pointsMove.length == 1) {
		pointsMove.pop();
	}
	redrawAll();
};

// Calculation and Drawing (embedding of the code from Ivan Kutskir and calling his methods to calculate the voronoi and delaunay)

function redrawAll() {
	draw.clear();
	// clear edges
	var vedges = [];

	// combine fixed (dataPoints) and moving (pointsMove)
	var allPoint = dataPoints.concat(pointsMove);
	// draw fixed and moving points
	drawPoints(allPoint);
	
	// if there are no fixed points --> there is at best one moving point --> not enough points to calculate a voronoi --> leave function redrawAll (else it would draw the previous calculated voronoi)
	if (dataPoints.length == 0) {
		return;
	}
	
	// --> not enough points to calculate a voronoi --> leave function redrawAll (else it would draw the previous calculated voronoi)
	if (allPoint.length == 1) {
		return;
	}
	
	// getting all points into the Point-class format (Voronoi needs this format)
	var voronoiPoints = new Array();
	for(i=0; i<allPoint.length; i++){
		var p = allPoint[i];
		voronoiPoints.push( new Point(p.x, p.y));
	}

	// computing voronoi and delaunay
	v.Compute(voronoiPoints, size_x, size_y);
	// getting calculated edges (containing voronoi edges and delaunay lines)
	vedges = v.GetEdges();

	// if toggle drawingVoronoi is true --> call function to draw voronoi edges (borders of voronoi cells)
	if (drawingVoronoi) {
		drawEdges(vedges);
	}

	//if toggle drawingDelaunay is true --> call function to draw delaunay lines
	if (drawingDelaunay) {
		drawDelaunay(vedges);
	}
}

function drawPoints(pt) {
	for(i=0; i<pt.length; i++){
		var p = pt[i];
		draw.circle(diameter).attr({ cx: p.x, cy: p.y, fill: dataColor });
	}
}

function drawEdges(edges) {
	for(i=0; i<edges.length; i++){
		var e = edges[i];
		draw.line(e.start.x, e.start.y, e.end.x, e.end.y).stroke({ color: '#0065bd', width: 3 });
	}
}

function drawDelaunay(edges) {
	for(i=0; i<edges.length; i++){
		var e = edges[i];
		draw.line(e.left.x, e.left.y, e.right.x, e.right.y).stroke({ color: '#e37222', width: 3 })
	}
}

//==============================================================================
//  Helper
//==============================================================================

function copy(a) {
  return JSON.parse(JSON.stringify(a));
}
